#include "stm8s.h"
#include "zpozdeni_1_sekunda.h"
#include "sekundy_0-9.h"

//Definování funkcí START/STOP a RESET

uint8_t stop = 0; //Proměnná potřebná pro start a stop

uint8_t Reset_Tlacitko_Zmacknuti(void)
{
    return (GPIO_ReadInputPin(GPIOE, GPIO_PIN_4) == 0);
}

//Reset jednotlivých segmentů (Sek, Dek, Sto, Tis)
int reset_Sek(uint8_t sekundy)
{
    if (Reset_Tlacitko_Zmacknuti()) //Pokud zmáčkneme tlačítko výše definované...
    {
        sekundy = 0; //Nastav hodnotu sekund na 0
        vykresleniSekundy(sekundy); //Vykresli na sedmisegmentu danou hodnotu
        
    }

    return sekundy; //Vrátí hodnotu sekund
}

int reset_Dek(uint8_t dekadicky) //Stejný princip jako u sekund, pouze u druhého, desítkového sedmisegmentu
{
    if (Reset_Tlacitko_Zmacknuti())
    {
        dekadicky = 0;
        vykresleniDekadicky(dekadicky);
    }

    return dekadicky;
}

int reset_Sto(uint8_t stovkove)
{
    if (Reset_Tlacitko_Zmacknuti())
    {
        stovkove = 0;
        vykresleniStovkove(stovkove);
    }

    return stovkove;
}

int reset_Tis(uint8_t tisicovkove)
{
    if (Reset_Tlacitko_Zmacknuti())
    {
        tisicovkove = 0;
        vykreslenTisicovkove(tisicovkove);
    }

    return tisicovkove;
}


//Funkce START/STOP tlačítka
uint8_t Stop_Tlacitko_Zmacknuti(void)
{
    return (GPIO_ReadInputPin(GPIOD, GPIO_PIN_2) == 0);
}

int Stop_Start(void)
{
    if (Stop_Tlacitko_Zmacknuti()) //Pokud stiskneme tlačítko PD2
    {

        if (stop == 1) // pokud jsou stopky zastavené...
        {
            stop = 0; // ...stopky budou pokračkovat v počítání
        }
        else // pokud jsou stopky v běhu...
        {
            stop = 1; // ...zastaví se
        }

    }
    if (stop == 1) //Zajišťuje zastavení stopek
    { 
        while (!(Stop_Tlacitko_Zmacknuti()))
        {

        }

    }
}

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    delay_timer_init(); //Inicializace delay funkce
  
    //Inicializace všech potřebných pinů na výstupy a výjimečně i výstupy
    GPIO_Init(GPIOC, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_0, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_1, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_3, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_6, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_7, GPIO_MODE_OUT_PP_HIGH_SLOW);
    
    GPIO_Init(GPIOG, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOA, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOF, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);

    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_NO_IT); //Inicializace tlačítka RESET
    GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT); //Inicializace tlačítko START/STOP


    //Definování proměnných, které mi pomohou zvedat čísla na sedmisegmentech
    uint8_t sekundy = 0;
    uint8_t dekadicky = 0;
    uint8_t stovkove = 0;
    uint8_t tisicovkove = 0;

    
    delay_1_sec(1);
    vykresleniSekundy(sekundy);
    vykresleniDekadicky(dekadicky);
    vykresleniStovkove(stovkove);
    vykreslenTisicovkove(tisicovkove);
    delay_1_sec(1);

    //Hlavní program
    while(1)
    {    
        sekundy = reset_Sek(sekundy); //Do proměnné se přiřadí hodnota, pokud je stisknuté příslušné tlačítko RESET
        dekadicky = reset_Dek(dekadicky);
        stovkove = reset_Sto(stovkove);
        tisicovkove = reset_Tis(tisicovkove);
        sekundy = sekundy + 1; 
        sekundy = reset_Sek(sekundy);
        vykresleniSekundy(sekundy); //Vyobrazení všech sedmisegmentů
        vykresleniDekadicky(dekadicky);
        vykresleniStovkove(stovkove);
        vykreslenTisicovkove(tisicovkove);
        delay_1_sec(1);

        //Kontrola hodnot na příslušném sedmisegmentu, kde kontrolujeme, jestli můžeme zvýšit hodnotu na sedmisegment
        if ((tisicovkove < 5) & (stovkove == 9) & (dekadicky == 5) & (sekundy == 9)) //Pokud je čtvrtý sedmisegment o hodnotě 5 (50. minuta) a třetí sedmisegment o hodnotě 9, (9. minuta) a zaroveň je druhý sedmisegment na hodnotě 5 (50 sekund), zvyš hodnotu která se zobrazuje na sedmisegmentu.
        {
            tisicovkove = tisicovkove + 1; //Zvýšení hodnoty sedmisegmentu o jedno.
        }
    
        else if ((tisicovkove == 5) & (stovkove == 9) & (dekadicky == 5) & (sekundy == 9)) //Pokud je čas 59 minut a 50 (9) sekund, vynuluj 4. sedmisegment.
        {
            tisicovkove = 0; //Nastavení zobrazované hodnoty na nulu
            stovkove = 0; //Vynuluj sedmisegment
            dekadicky = 0;//Vynuluje se
            sekundy = 0;//Vynuluje se
            vykresleniSekundy(sekundy);
            vykresleniDekadicky(dekadicky);
            vykresleniStovkove(stovkove);
            vykreslenTisicovkove(tisicovkove);
            delay_1_sec(1);
        }    
 
        if ((stovkove < 9) & (dekadicky == 5) & (sekundy == 9)) // Pokud je hodnota menší než 9 minut a hodnota je 50 (9) sekund,
        {
            stovkove = stovkove + 1; //Zvyš hodnotu 3. sedmisegmentu o jednu.
        }

        else if ((stovkove == 9) & (dekadicky == 5) & (sekundy == 9)) //Pokud je třetí sedmisemgent na 9 minutách a 50 (9) sekundách,
        {
            stovkove = 0; //Vynuluj sedmisegment
            dekadicky = 0;//Vynuluje se
            sekundy = 0;//Vynuluje se
            vykresleniSekundy(sekundy);
            vykresleniDekadicky(dekadicky);
            vykresleniStovkove(stovkove);
            vykreslenTisicovkove(tisicovkove);
            delay_1_sec(1);
        } 

        if ((dekadicky < 5) & (sekundy == 9)) //Pokud je druhý sedmisegment menší než 50 (9) sekund
        {
            dekadicky = dekadicky + 1; //Zvyš o jedno
        }
            
        else if ((dekadicky == 5) & (sekundy == 9)) //Pokud druhý sedmisegment dosáhne hodnoty 50 (9) sekund,
        {
            dekadicky = 0;//Vynuluje se
            sekundy = 0;//Vynuluje se
            vykresleniSekundy(sekundy);
            vykresleniDekadicky(dekadicky);
            vykresleniStovkove(stovkove);
            vykreslenTisicovkove(tisicovkove);
            delay_1_sec(1);
        }
            
        if (sekundy == 9)  //Pokud první sedmisegment dosáhne hodnoty 9 sekund,
        {
            sekundy = 0; //Vynuluje se
            vykresleniSekundy(sekundy);
            vykresleniDekadicky(dekadicky);
            vykresleniStovkove(stovkove);
            vykreslenTisicovkove(tisicovkove);  
            delay_1_sec(1);
        }
    }
}