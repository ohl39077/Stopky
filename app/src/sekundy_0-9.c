#include "sekundy_0-9.h"
#include "zpozdeni_1_sekunda.h"

//Nastavování pinů na hodnotu potřebnou pro zobrazení požadovaného čísla

uint8_t vykresleniSekundy(uint8_t sekundy)
{
    //Fuknce prvního sedmisegmentu
    if (sekundy == 0)
    {
        Nula_7SEG_1();
    }

    else if (sekundy == 1)
    {
        Jedna_7SEG_1();
    } 

    else if (sekundy == 2)
    {
        Dva_7SEG_1();
    }

    else if (sekundy == 3)
    {
        Tri_7SEG_1();
    }

    else if (sekundy == 4)
    {
        Ctyri_7SEG_1();
    }

    else if (sekundy == 5)
    {
        Pet_7SEG_1();
    }

    else if (sekundy == 6)
    {
        Sest_7SEG_1();
    }

    else if (sekundy == 7)
    {
        Sedm_7SEG_1();
    }

    else if (sekundy == 8)
    {
        Osm_7SEG_1();
    }

    else if (sekundy == 9)
    {
        Devet_7SEG_1();
    }

    return sekundy;
}

uint8_t vykresleniDekadicky(uint8_t dekadicky)
{
    //Funkce druhého sedmisegmentu
    if (dekadicky == 0)
    {
        Nula_7SEG_2();
    }    
    
    else if (dekadicky == 1)
    {
        Jedna_7SEG_2();
    }
    else if (dekadicky == 2)
    {
        Dva_7SEG_2();
    }
    else if (dekadicky == 3)
    {
        Tri_7SEG_2();
    }

    else if (dekadicky == 4)
    {
        Ctyri_7SEG_2();
    }

    else if (dekadicky == 5)
    {
        Pet_7SEG_2();
    }

    return dekadicky;
}

uint8_t vykresleniStovkove(uint8_t stovkove)
{
    //Fuknce třetího sedmisegmentu
    if (stovkove == 0)
    {
        Nula_7SEG_3();
    }
    else if (stovkove == 1)
    {
        Jedna_7SEG_3();
    } 
    else if (stovkove == 2)
    {
        Dva_7SEG_3();
    }
    else if (stovkove == 3)
    {
        Tri_7SEG_3();
    }
    else if (stovkove == 4)
    {
        Ctyri_7SEG_3();
    }
    else if (stovkove == 5)
    {
        Pet_7SEG_3();
    }
    else if (stovkove == 6)
    {
        Sest_7SEG_3();
    }
    else if (stovkove == 7)
    {
        Sedm_7SEG_3();
    }
    else if (stovkove == 8)
    {
        Osm_7SEG_3();
    }
    else if (stovkove == 9)
    {
        Devet_7SEG_3();
    }

    return stovkove;
}


uint8_t vykreslenTisicovkove(uint8_t tisicovkove)
{
    //Funkce 4. sedmisegmentu:
    //Pokud je proměnná 4. sedmisegmentu na hodnotě nula, zobrazují číslo nula
    if (tisicovkove == 0)
    {
        Nula_7SEG_4();
    }
    //Pokud je proměnná 4. sedmisegmentu na hodnotě jedna, zobrazují číslo jedna
    else if (tisicovkove == 1)
    {
        Jedna_7SEG_4();
    }
    else if (tisicovkove == 2)
    {
        Dva_7SEG_4();
    }
    else if (tisicovkove == 3)
    {
        Tri_7SEG_4();
    }
    else if (tisicovkove == 4)
    {
        Ctyri_7SEG_4();
    }
    else if (tisicovkove == 5)
    {
        Pet_7SEG_4();
    }
    
    return tisicovkove;
}    

void Nula_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);

}

void Jedna_7SEG_1(void)
{
    GPIO_WriteHigh(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
}

void Dva_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);


}

void Tri_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}

void Ctyri_7SEG_1(void)
{
    GPIO_WriteHigh(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}

void Pet_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
}

void Sest_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}


void Sedm_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);

}

void Osm_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}

void Devet_7SEG_1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
}

void VYCISTIT_7SEG_1(void)
{
    GPIO_WriteHigh(GPIOC, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);

}

/* druhy sedmisegment */

void Nula_7SEG_2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
}

void Jedna_7SEG_2(void)
{
    GPIO_WriteHigh(GPIOE, GPIO_PIN_0);
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
}

void Dva_7SEG_2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_2);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
}

void Tri_7SEG_2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
}

void Ctyri_7SEG_2(void)
{
    GPIO_WriteHigh(GPIOE, GPIO_PIN_0);
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
}

void Pet_7SEG_2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
}

/* treti sedmisegment */

void Nula_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
}

void Jedna_7SEG_3(void)
{
    GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
}

void Dva_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

void Tri_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

void Ctyri_7SEG_3(void)
{
    GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

void Pet_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

void Sest_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

void Sedm_7SEG_3(void)
{
    //GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    //GPIO_WriteLow(GPIOA, GPIO_PIN_2);
    //GPIO_WriteLow(GPIOA, GPIO_PIN_3);
    //GPIO_WriteHigh(GPIOA, GPIO_PIN_4);
    //GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    //GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
    //GPIO_WriteHigh(GPIOF, GPIO_PIN_7);
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
}

void Osm_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

void Devet_7SEG_3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOF, GPIO_PIN_0);
}

/* ctvrty sedmisegment */

void Nula_7SEG_4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_0);
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_6);
}

void Jedna_7SEG_4(void)
{
    GPIO_WriteHigh(GPIOB, GPIO_PIN_0);
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_6);
}

void Dva_7SEG_4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_0);
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
}

void Tri_7SEG_4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_0);
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
}

void Ctyri_7SEG_4(void)
{
    GPIO_WriteHigh(GPIOB, GPIO_PIN_0);
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
}

void Pet_7SEG_4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_0);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
}