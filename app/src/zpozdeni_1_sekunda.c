#include "zpozdeni_1_sekunda.h"

//Inicializace časovače
void delay_timer_init(void)
{
    TIM3_TimeBaseInit(TIM3_PRESCALER_256, 62499); //Předřadička 256, a C je 62499
    TIM3_Cmd(ENABLE); //Aktivace
}
//Funkce na zpoždění 1 sekundy
void delay_1_sec(uint32_t time_s)
{
    TIM3_SetCounter(0); //Nastavení časovače na hodnotu 0 sekund
    for(uint32_t s = 0; s < time_s; s++)
    {
        while(TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET) //Kontrola vlajky
        {
            Stop_Start(); //Spuštění funkce zastavování stopek
        }
        TIM3_ClearFlag(TIM3_FLAG_UPDATE); //Vymazání vlajky
    }
}
