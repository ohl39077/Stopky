#ifndef INC_SEDMISEGMENT1_H
#define INC_SEDMISEGMENT1_H

#include "stm8s.h"

uint8_t vykresleniSekundy(uint8_t sekundy);
uint8_t vykresleniDekadicky(uint8_t dekadicky);
uint8_t vykresleniStovkove(uint8_t stovkove);
uint8_t vykreslenTisicovkove(uint8_t tisicovkove);

void VYCISTIT_7SEG_1(void);
void Nula_7SEG_1(void);
void Jedna_7SEG_1(void);
void Dva_7SEG_1(void);
void Tri_7SEG_1(void);
void Ctyri_7SEG_1(void);
void Pet_7SEG_1(void);
void Sest_7SEG_1(void);
void Sedm_7SEG_1(void);
void Osm_7SEG_1(void);
void Devet_7SEG_1(void);

void Nula_7SEG_2(void);
void Jedna_7SEG_2(void);
void Dva_7SEG_2(void);
void Tri_7SEG_2(void);
void Ctyri_7SEG_2(void);
void Pet_7SEG_2(void);

void Nula_7SEG_3(void);
void Jedna_7SEG_3(void);
void Dva_7SEG_3(void);
void Tri_7SEG_3(void);
void Ctyri_7SEG_3(void);
void Pet_7SEG_3(void);
void Sest_7SEG_3(void);
void Sedm_7SEG_3(void);
void Osm_7SEG_3(void);
void Devet_7SEG_3(void);

void Nula_7SEG_4(void);
void Jedna_7SEG_4(void);
void Dva_7SEG_4(void);
void Tri_7SEG_4(void);
void Ctyri_7SEG_4(void);
void Pet_7SEG_4(void);
#endif