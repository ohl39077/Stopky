#ifndef INC_ZPOZDENI_H
#define INC_ZPOZDENI_H

#include "stm8s.h"

void delay_timer_init(void);
void delay_1_sec(uint32_t time_s);

#endif