# Sedmisegmentové stopky

## 📢Úvodem...

Jako můj ročníkový projekt jsem si v rozličném množství témat nakonec vybral Stopky. Projekt jsem si vybral pro jeho vysokou prakticitu v praxi a jeho snadnou rozšířitelnost o nové funkce.

Při návrhu celého zapojení jsem často čerpal z 2. ročníku číslicové techniky, kde jsme používali sedmisegmentové zobrazovače. Vybral jsem si je pro jejich příočaré a všestranné použití, které se mi nepochybně bude hodit do budoucí praxe v oboru - očekávám, že časem budu využívat mnohem komplexnějších zobrazovačů. STM8 Nucleo-8S208RB nám pro tvorbu takovýchto projektů nabízí platformu společně s jeho jednoduchostí pochopení jeho základních funkcí.

## 👩‍🔬Popis funkce:
Ve výsledku je můj projekt schopen počítat až do délky jedné hodiny. To znamená, 59 minut a 59 sekund. To zajišťují 4 rozdílné sedmisegmenty. Časování jsme dosáhli pomocí interních časovačů, které jsem naprogrmaoval na delay 1 sekundy. Při stisknutí tlačítka START/STOP při výchozím spustění se nám spustí časování, které postupně rozsvicuje sedmisegmenty podle uběhlého času. Spouštění a zastavování je ovládáno pomocí tlačítka. Stejě tak nulování, které nám umožňuje kompletní vynulovaní průběhu časovaní a možnost začít časovat znovu.
## 🖍 Blokové schéma:

Tento nákres vyobrazuje principiální zapojení Stopek. Je  uřčené přimárně pro rychlé pochopení zapojení.

![Blokové schéma tvořené pomocí mermaid](diagram.png)

## 📐KiCAD schéma:

Komplexní schéma zapojení, díky kterému jsme schopni replikovat zapojení.

Tento obrázek je pouze ochutnávka. Zbytek zapojení je možné nalézt v přiloženém souboru [PDF](KiCAD_schema.pdf)

![Schéma zapojení v programu KiCAD](kicad_obr.png)



## 👩‍💻 Seznam součástek

| Typ součastky    |Reference| Hodnota | Počet kusů |  Cena za kus |  Cena dohromady |
|:----------------:|:---------:|:-------:|:----------:|:------------:|:---------------:|
| Sedmisegmenty 1-4 |[Zde](https://www.svetsoucastek.cz/led-displej-wenrun-lsd030bue-10a-02-p135361/)|LSD030BUE-10A-02|4 ks|     15 Kč|60 Kč|
|     Kabely       |[Zde](https://www.svetsoucastek.cz/propojovaci-kabliky-pro-arduino-a-nepajiva-kontaktni-pole-sada-65ks-p56880/)    |   M-M, F-M, F-F        |      1 set     |       149 Kč      | 149 Kč|
|     Tlačítka     |   [Zde](https://www.svetsoucastek.cz/tlacitkovy-spinac-highly-ks01-bmbk-p54712/)  |     Spínací     |      2 Ks     |     15Kč      | 30 Kč|
| STM8 Nucleo      |     [Zde](https://cz.mouser.com/ProductDetail/STMicroelectronics/NUCLEO-8S208RB?qs=w%2Fv1CP2dgqrd8EC5ib0VDw%3D%3D)    |      -    |    1Kč     |      255Kč      | 255 Kč|
| Celkem:          |         |            |              |           | 249 Kč (494 Kč s STM8)|

## 📷Fotografie
### Zapojení na nepájivém poli:
![Nepájivé pole obr](projekt_obr1.jpg)

### Zapojení na nepájivém poli společně s STM8:
![Nepájivé pole obr 2](projekt_obr3.jpg)

### Spuštěné finální zapojení:
![Nepájivé pole obr 3](projekt_obr2.jpg)
## ⭐Závěr

Celková doba tvorby projektu se blíží ke sto hodinám. Nejdéle mi trvalo napsat program samotný - Postupoval jsem následovně: Nejdříve jsem naprogtamoval časování samotné, později vložil funkci zastavení programu a naposledy nulování, které se ukázalo být nejnáročnější a nejnáchylnější na error - jak lidský, tak technologický. Nejjednodušší mi přišla logika v programu, která byla přímočará a jednoduše srozumitelná. V projektu jsme měli možnost využít nabité znalosti z předchozích hodin jako napříkad: Vlajky, Časovače, return atd. Pro přístí, mírně pokročilejší zapojení využiji dekodérů BCD, které by mi zjednodušily návrh. 

## Referenční materiál pro zájemce:
- [STM8 Datasheet](https://www.st.com/resource/en/user_manual/dm00477617-stm8s208rbt6-nucleo64-board-stmicroelectronics.pdf)
- [Obchod, kde jsem nakoupil součástky](https://www.svetsoucastek.cz/)
- [YouTube Video](https://youtube.com/shorts/eHbjTyL1opc?feature=share) <-- Opravena chyba zobrazování při RESETU, ale už není dost světla na nové video. Omlouvám se.
